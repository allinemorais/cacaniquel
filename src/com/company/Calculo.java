package com.company;

import java.util.ArrayList;

public class Calculo {

    public int calculo(ArrayList<String> lstSorteio)
    {
        int resultado = 0;
        int repeticao = 1;
        ArrayList<String> figurasRepetidas = new ArrayList<>();

        for (String sorteio : lstSorteio)
        {
            System.out.println(sorteio + "------- valor --"  + Figuras.valueOf(sorteio).getValores() );

            if(figurasRepetidas.contains(sorteio))
            {
                repeticao = repeticao + 1;
            }

            figurasRepetidas.add(sorteio);

            resultado = resultado + Figuras.valueOf(sorteio).getValores();
        }


        if(repeticao == 3)
        {
            resultado = resultado * 100;
        }

        return resultado;
    }
}
